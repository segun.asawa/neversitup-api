const express = require('express')
const app = express()
const mongoose = require('mongoose')
const User = require('./models/user')
const Product = require('./models/product')
const Orderdetail = require('./models/orderdetail')

const mongo_uri = "mongodb+srv://admin:admin@ecommerce.ow43u.mongodb.net/neversitup?retryWrites=true&w=majority"
mongoose.connect(mongo_uri, { useNewUrlParser: true })

app.use(express.json())


// create user
app.post('/user/create', async (req, res) => {
  const user = new User(req.body)
  try {
    await user.save()
    res.send(user)
  } catch (err) {
    res.status(500).send(err)
  }
})

// login
app.post('/login', async (req, res) => {
  const result = await User.find({ username: req.body.username })
  if (result) {
    res.json({
      isCorrect: result[0].password === req.body.password
    })
  } else {
    res.json({
      isCorrect: false,
    })
  }  
})

// user profile
app.get('/user/:id', async (req, res) => {
    const { id } = req.params
    const result = await User.find({ userId: id })
    res.json(result)
})

// user profile order history
app.get('/user/:id/order', async (req, res) => {
  const { id } = req.params
  const result = await Orderdetail.find({ userId: id })
  res.json(result)
})

// all product
app.get('/product/all', async (req, res) => {
  const result = await Product.find()
  res.json(result)
})

// product info
app.get('/product/:id', async (req, res) => {
  const { id } = req.params
  const result = await Product.find({ productId: id })
  res.json(result)
})

// create order
app.post('/order/create', async (req, res) => {
  const order = new Orderdetail(req.body)
  try {
    await order.save()
    res.send(order)
  } catch (err) {
    res.status(500).send(err)
  }
})

// cancle order
app.post('/order/cancel/:id', async (req, res) => {
  const { id } = req.params
  try {
    await Orderdetail.findOneAndUpdate({ orderId: id }, { isCancel: true }).exec()
    res.send(id)
  } catch (err) {
    res.status(500).send(err)
  }
})

// order detail
app.get('/order/:id', async (req, res) => {
  const { id } = req.params
  const result = await Orderdetail.find({ orderId: id })
  res.json(result)
})

app.listen(3000, () => {
console.log('Application is running on port 3000')
console.log('http://localhost:3000')
})
