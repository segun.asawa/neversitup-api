const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  userId : {
    type: Number,
    unique : true,
    required: [true, 'userId is required']
  },
  username : {
    type: String,
    unique : true,
    required: [true, 'username is required']
  },
  password : {
    type: String,
    required: [true, 'password is required']
  },
  email : {
    type: String,
    unique : true,
    required: [true, 'email is required']
  },
  mobile : {
    type: String,
    unique : true,
    required: [true, 'mobile is required']
  },
  firstname : {
    type: String,
    required: [true, 'firstname is required']
  },
  lastname : {
    type: String,
    required: [true, 'lastname is required']
  },
  gender : {
    type: String,
    required: [true, 'gender is required']
  },
  address : {
    type: String
  }
})

const userModel = mongoose.model('user', userSchema, 'user')

module.exports = userModel

