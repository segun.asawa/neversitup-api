const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
  productId : {
    type: Number,
    unique : true,
    required: [true, 'productId is required']
  },
  productName : {
    type: String,
    required: [true, 'productName is required']
  },
  productPrice : {
    type: Number,
    required: [true, 'productPrice is required']
  },
  producQuantity : {
    type: Number,
    required: [true, 'producQuantity is required']
  },
  productOrdered : {
    type: Number,
    required: [true, 'productOrdered is required']
  },
  productStatus : {
    type: Boolean,
    required: [true, 'productStatus is required']
  }
})

const ProductModel = mongoose.model('product', productSchema, 'product')

module.exports = ProductModel

