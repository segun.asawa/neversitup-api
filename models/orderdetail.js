const mongoose = require('mongoose')

const orderdetailSchema = new mongoose.Schema({
  orderId : {
    type: Number,
    unique : true,
    required: [true, 'orderId is required']
  },
  userId : {
    type: Number,
    required: [true, 'userId is required']
  },
  productId : {
    type: Number,
    required: [true, 'productId is required']
  },
  quantity : {
    type: Number,
    required: [true, 'quantity is required']
  },
  totalPrice : {
    type: Number,
    required: [true, 'totalPrice is required']
  },
  orderDate : {
    type: Date,
    required: [true, 'orderDate is required']
  },
  shippedDate : {
    type: Date,
    required: [true, 'shippedDate is required']
  },
  shipAddress : {
    type: String,
    required: [true, 'shipAddress is required']
  },
  isCancel : {
    type: Boolean,
    required: [true, 'orderStatus is required']
  }
})

const orderdetailModel = mongoose.model('orderdetail', orderdetailSchema, 'orderdetail')

module.exports = orderdetailModel

